package ru.dalimo.akimovan.mobilewms;

import android.app.Activity;
import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Toxo on 05.03.2018.
 */

public class Constants {
    public static final String CHARSET_UTF16LE = "UTF-16LE";
    public static final String CHARSET_UTF16BE = "UTF-16BE";
    public static final String CHARSET_UTF8 = "UTF-8";
    public static final String CHARSET_ASCII = "US-ASCII";
    public static final String KOI8_R = "KOI8-R";

    public static void makeToast(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showAnswer(final Activity activity, final TextView textView, final String answer) {
        if(activity!=null && answer!=null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setText(answer);
                }
            });}
    }
}
