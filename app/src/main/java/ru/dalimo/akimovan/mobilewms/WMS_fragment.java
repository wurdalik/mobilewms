package ru.dalimo.akimovan.mobilewms;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

/**
 * Created by Toxo on 04.03.2018.
 */

public class WMS_fragment extends Fragment implements OnCompleteTaskListener{
    private static final String TAG = "WMS_fragment";
    private EditText ipAddress;
    private EditText port;
    private EditText command;
    private CheckBox itsMessage;
    private Button connect;
    private TextView answerTextView;

    String server = "10.0.70.13";
    String port_number = "2050";
    String message = new String("9T;0001;");

    private ConnectToServer mServer;
    private ServerInfo mServerInfo;
    private WMSClient wmsClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.wms_fragment, null);
        ipAddress = v.findViewById(R.id.ip_address);
        port = v.findViewById(R.id.port);
        command = v.findViewById(R.id.command);
        itsMessage = v.findViewById(R.id.its_message);
        answerTextView = v.findViewById(R.id.answer);
        connect = v.findViewById(R.id.connect);
        ipAddress.setText(server);
        port.setText(port_number);
        command.setText(message);
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answerTextView.setText("");
                mServerInfo = new ServerInfo(ipAddress.getText().toString(), Integer.parseInt(port.getText().toString()));
                wmsClient = new WMSClient(getActivity(), mServerInfo);
                String message = command.getText().toString();
                boolean itsmessage = itsMessage.isChecked();
                    try {
                        mServer = new ConnectToServer(getActivity().getBaseContext(), wmsClient, message, itsmessage, WMS_fragment.this);
                        mServer.start();
                    } catch (IOException e) {
                        Log.e(TAG, "Не удалось подключиться: "+ e.toString() );
                    }
                }
        });
        return v;
    }

    @Override
    public void onTaskComplete(ConnectToServer connectToServer) {
        String answer = connectToServer.getAnswer();
        Constants.showAnswer(getActivity(), answerTextView, answer);
    }
}
