package ru.dalimo.akimovan.mobilewms;

import java.net.Socket;

/**
 * Created by Toxo on 04.03.2018.
 */

public class ServerInfo extends Socket{
    private String ip;
    private int port;

    public ServerInfo(String ip, int port){
        this.ip = ip;
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
