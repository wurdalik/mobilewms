package ru.dalimo.akimovan.mobilewms;

import android.content.Context;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Toxo on 05.03.2018.
 */

public class WMSClient {
    public static final String TAG = "WMSClient";
    private Socket socket;
    private Context context;
    private ServerInfo mServerInfo;
    private InputStream
            in = null;
    private OutputStream out = null;

    public WMSClient(Context context, ServerInfo mServerInfo){
        this.context = context;
        this.mServerInfo = mServerInfo;
    }

    private boolean createSocket(String ip, int port){
        try {
            socket = new Socket();
            InetSocketAddress inetAddress = new InetSocketAddress(ip, port);
            socket.connect(inetAddress);
        } catch (IOException e) {
            Log.e(TAG, "Ошибка создания сокета "+ e.toString());
            return false;
        }
        return true;
    }

    public boolean connect(){
        if(mServerInfo!=null) {
            if (mServerInfo.getIp() != null) {
                if (mServerInfo.getPort() != 0) {

                } else {
                    Constants.makeToast(context, "Введите порт");
                }
            } else {
                Constants.makeToast(context, "Введите ip");
            }
        }
        else{
            Log.e(TAG, "Пустой адрес сервера и порт" );
        }
        String server = mServerInfo.getIp();
        int port = mServerInfo.getPort();
        if(!createSocket(server, port)){
            Log.i(TAG, "Не удалось подключится");
        }
        if(socket.isConnected()){
            try {
                in = socket.getInputStream();
                out = socket.getOutputStream();
            } catch (IOException e) {
                Constants.makeToast(context, "Не удалось создать поток чтения/записи"+ e.toString());
                Log.e(TAG, "Не удалось создать поток чтения/записи"+ e.toString() );
                disconnect();
                return false;
            }
        }
        Log.e(TAG, "Подключение успешно");
        return true;
    }

    public void disconnect(){
        try {
            if(in!=null){
                in.close();
            }
            if(out!=null){
                out.close();
            }
            if(socket!=null){
                socket.close();}
        }catch (IOException e) {
            Log.e(TAG, "disconnect: "+ e.toString() );
            Constants.makeToast(context, e.toString());
        }
    }

    public void send(byte[] message) {
        send(message, Constants.CHARSET_UTF16LE);
    }

    public void send(byte[] message, String codePageName) {
        try {
            out.write(message);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            disconnect();
        }
    }

    public String receive() throws IOException {
        byte[] answer = new byte[200];
        BufferedInputStream bis = new BufferedInputStream(in);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        while (true){
            int readBytesCount = bis.read(answer);
            if(readBytesCount>0){
            baos.write(answer, 0, readBytesCount);
            break;
            }
        }
        baos.flush();
        baos.close();
        Log.e(TAG, "baos: "+ Arrays.toString(baos.toByteArray()) );
        return parce(baos.toByteArray());
    }

    private String parce(byte[]answer) throws UnsupportedEncodingException {
        byte[]type_answer;
        byte[]messageByte = null;
        String type = null;
        String message = null;

        for (int i = 0; i < answer.length; i++) {
            if(answer[i]==59){
                type_answer = Arrays.copyOfRange(answer, 0, i+1);
                type = delete_left(type_answer);
                if(type.contains("ERROR;")){
                    messageByte = Arrays.copyOfRange(answer, i+1, answer.length-1);
                    message = new String(messageByte, "UTF-16BE");
                    break;
                }
            }
        }
        String result = String.format("%s", message);
        byte[] test = new byte[]{4, 20, 4, 48, 4, 61, 4, 61, 4, 48, 4, 79, 0, 0, 4, 62, 4, 63, 4, 53, 4, 64, 4, 48, 4, 70, 4, 56, 4, 79,};
        String testString = new String(test, "UTF-16BE");
        Log.e(TAG, "parce: "+"сообщение:"+ message +"\n"+Arrays.toString(messageByte)+"\n"+"test"+testString);

        return result;
    }

    private String delete_left(byte[]type) throws UnsupportedEncodingException {
        List<Character>asset = new ArrayList<Character>(){
            {
                add('S');
                add('E');
                add('V');
                add('C');
                add('O');
                add('U');
                add('P');
                add('E');
                add('R');
                add('R');
                add('O');
                add('R');
                add(';');
            }
        };
        String parce = new String(type, "UTF-8");
        String result = "";
        char[] string = parce.toCharArray();
        for (int i = 0; i<=string.length-1;i++){
            if(asset.contains(string[i])){
                result+=Character.toString(string[i]);
            }
        }
        return result;
    }


















}
