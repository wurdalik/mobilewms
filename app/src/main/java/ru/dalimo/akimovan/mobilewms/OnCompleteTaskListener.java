package ru.dalimo.akimovan.mobilewms;

/**
 * Created by Toxo on 05.03.2018.
 */

public interface OnCompleteTaskListener {
    void onTaskComplete(ConnectToServer connectToServer);
}
