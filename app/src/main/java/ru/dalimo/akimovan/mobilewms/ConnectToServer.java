package ru.dalimo.akimovan.mobilewms;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

/**
 * Created by Toxo on 04.03.2018.
 */

public class ConnectToServer extends Thread {
    private static final String TAG = "ConnectToServer";
    private String answer;
    private WMSClient wmsClient;
    private Context context;
    private Socket socket;
    private String message;
    private boolean itsMessage;

    OnCompleteTaskListener onCompleteTaskListener;


    public String getAnswer() {
        return answer;
    }

    public ConnectToServer(Context context, WMSClient wmsClient, String message, boolean itsMessage, OnCompleteTaskListener onCompleteTaskListener) throws IOException {
        this.context = context;
        this.wmsClient = wmsClient;
        this.message = message;
        this.itsMessage = itsMessage;
        this.onCompleteTaskListener = onCompleteTaskListener;
    }

    @Override
    public void run() {
        if(wmsClient.connect()){
            try {
                wmsClient.send(prepareMessage(message, itsMessage));
                answer = new String(wmsClient.receive());
            } catch (IOException e) {
                e.printStackTrace();
            }

            wmsClient.disconnect();
        }
        onCompleteTaskListener.onTaskComplete(this);
    }

    public byte[]prepareMessage(String s, boolean ItsMessage) throws UnsupportedEncodingException {
        String generationID = GenerationID();
        String title = "sevco";
        String userID = "0001";

        String helper;

        ByteBuffer message;

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String date = dateFormat.format(new Date());

        String hash = String.format("%s_%s_%s", s.hashCode(), generationID, date);

        byte[] myTempBuf;      // Result byte array

        byte[] titleBuff;        // Title
        byte[] userIDBuff;       // User Id
        byte[] buffMLeng;        // MAC Address length
        byte[] buffMAC;          // MAC Address
        byte[] hashbuff;         // hash
        byte[] hashbuffleng;     // hash length
        byte[] databuff;         // data
        byte[] databuffleng;     // data length
        byte[] datatype;         // data type

        if(ItsMessage){
            title = "MESAG";
        }

        if(userID != null && userID !=""){
            userIDBuff = userID.getBytes(Charset.forName(Constants.CHARSET_ASCII));
        }
        else{
            userIDBuff = new byte[4];
        }

        titleBuff = title.getBytes(Charset.forName(Constants.CHARSET_ASCII));
        buffMLeng = BigInteger.valueOf(getMacAddress().length).toByteArray();
        buffMAC = getMacAddress();
        hashbuffleng = BigInteger.valueOf(hash.getBytes(Charset.forName(Constants.CHARSET_ASCII)).length).toByteArray();
        hashbuff = hash.getBytes(Charset.forName(Constants.CHARSET_ASCII));
        datatype = new byte[]{0};
        databuffleng = new byte[]{Integer.valueOf(s.getBytes(Constants.CHARSET_UTF16LE).length).byteValue(), 0, 0, 0};
        databuff = s.getBytes(Constants.CHARSET_UTF16LE);
        int length = titleBuff.length+userIDBuff.length+buffMLeng.length+buffMAC.length+hashbuffleng.length+hashbuff.length+datatype.length+databuffleng.length+databuff.length;
        message = ByteBuffer.allocate(length);

        message.put(titleBuff);
        message.put(userIDBuff);
        message.put(buffMLeng);
        message.put(buffMAC);
        message.put(hashbuffleng);
        message.put(hashbuff);
        message.put(databuffleng);
        message.put(datatype);
        message.put(databuff);

        myTempBuf = message.array();

        Log.i(TAG, "SendToServer: "+ Arrays.toString(myTempBuf) +"\n"+"длина "+length+" длина массива "+myTempBuf.length+ " длина данных "+ databuff.length + " данные "+ Arrays.toString(databuffleng));

        return myTempBuf;
    }

    public byte[] getMacAddress(){
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifiManager.getConnectionInfo();
        String mac = wInfo.getMacAddress();
        return mac.getBytes(Charset.forName("US-ASCII"));
    }

    private String GenerationID()
    {
        String _IDString = "";
        byte[] _ID = new byte[10];
        Random yr = new Random();
        yr.nextBytes(_ID);
        for (int i = 0; i < _ID.length; i++)
        {
            _IDString += String.valueOf(_ID[i]);
        }

        return _IDString;
    }


    }
